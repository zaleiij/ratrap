from bottle import route, run, template
from os import environ


@route('/salut')
def wel():
    return 'yes'


@route('/')
def index():
    return 'coucou la famille'


@route('/multiply/<a>/<b>')
def route_multiply(a, b):
    result = multiply(a, b)
    return template('<b>Result of {{a}} * {{b}} is {{result}}</b>!', a=a, b=b, result=result)


def multiply(a, b):
    return int(a) * int(b)


if __name__ == '__main__':
    run(host='0.0.0.0', port=environ.get(('PORT', 8080)))
